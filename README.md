# Graviton

![](https://raw.githubusercontent.com/zacharyfmarion/Material-Browser/master/Image1.png?raw=true "Graviton")

Graviton is a minimalist web browser for Mac made using [Electron](http://electron.atom.io/) and [Polymer](https://www.polymer-project.org/), and is still under heavy development.

## Build instructions

Navigate to a parent directory and clone into it:

`git clone https://github.com/zacharyfmarion/Material-Browser.git`

Install Dependecies:

`npm install && bower install`

Run gulp to build a distribution version

`gulp`

To start the app, run

`npm start`

Once a stable build is reached I will package it using Electron and provide a download link.

## Contributing

All pull requests are welcome!
